var express = require('express')
var cors = require('cors')
const mysql = require('mysql2');
const PORT = process.env.PORT || 5000

const connection = mysql.createConnection({
  host: '157.245.59.56',
  port: '3366',
  user: '6301942',
  password: '6301942',
  database: '6301942'
});

var app = express()
app.use(cors())
app.use(express.json())

app.get('/course', function (req, res, next) {
  connection.query(
    'SELECT * FROM `course`',
    function(err, results, fields) {
      res.json(results);
    }
  );
})

app.get('/course/:id', function (req, res, next) {
  const id = req.params.id;
  connection.query(
    'SELECT * FROM `course` WHERE `id` = ?',
    [id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.post('/course', function (req, res, next) {
  connection.query(
    'INSERT INTO `course`(`name`, `price`, `fname`, `lname`, `avatar`) VALUES (?, ?, ?, ?, ?)',
    [req.body.name, req.body.price, req.body.fname, req.body.lname, req.body.avatar],
    function(err, results) {
      res.json(results);
    }
  );
})

app.put('/course', function (req, res, next) {
  connection.query(
    'UPDATE `course` SET `name`= ?, `price`= ?, `fname`= ?, `lname`= ?, `avatar`= ? WHERE id = ?',
    [req.body.name, req.body.price, req.body.fname, req.body.lname, req.body.avatar, req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.delete('/course', function (req, res, next) {
  connection.query(
    'DELETE FROM `course` WHERE id = ?',
    [req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.listen(PORT, function () {
  console.log('CORS-enabled web server listening on port '+PORT)
})
